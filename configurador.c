/**********************************************************************************************
 * 											      *
 *	MUESTRA UNA INTERFAZ GRAFICA PARA QUE EL USUARIO SELECCIONE LOS DIRECTORIOS QUE       *
 *	DESEA RESPALDAR Y EL DIRECTORIO DONDE SE COPIARAN CADA VEZ QUE SE APAGUE EL           *
 *      ORDENADOR  Y METE LAS RUTAS DE LOS MISMOS EN LOS FICHEROS DE TEXTO: origenes.txt      *
 *      Y destino.txt RESPECTIVAMENTE							      *
 *											      *
 *********************************************************************************************/


#include <gtk/gtk.h>
#include "GTKGUI.h"
#include <unistd.h>

/*  Aplica el fichero de estilo CSS a los widgets   */
void myCSS(void); 


/*  Cabeceras de los manejadores  */
void on_btAceptar_clicked(); //Guarda los datos de configuracion introducidos en ficheros de texto.
void on_btDirectorios_clicked(); //Muestra el dialogo para añadir directorios.
void on_btEliminar_clicked(); //Para eliminar una ruta de directorio anteriormente añadida
void on_btDestino_clicked(); //Muestra el dialogo para añadir el directorio donde se guardaran 
			     // los directorios a respaldar.

// Declaracion de widgets 

GtkWidget *window, *VbDivisionesVentana,*HbMensajesDatos,*VbBotonAceptar,*HbDatos, *VbBotonDirectorios,*VbRutas,*VbBotonesRutas,*HbDestino,*VbBotonDestino; //Contenedores   
   GtkWidget *lbMensajes, *lb1,*lb2,*lb3,*lb4,*lb5,*lbDestino; //Etiquetas  
   GtkWidget *btAceptar, *btDirectorios,*bt1,*bt2,*bt3,*bt4,*bt5,*btDestino; //Botones
   GtkWidget *dialogo; //FileChooserDialog
    
//Variables globales   
int ancho,alto; //Para las dimensiones de la ventana contenedora
int indices [5] ; //Para marcar cuando una etiqueta de ruta de directorio esta rellena o no


int main(int argc, char * argv[])
{       	
    gtk_init(&argc, &argv);
    myCSS();

//Obtener la altura y anchura de la pantalla
  GdkDisplay * pantalla;
  pantalla = gdk_display_get_default();  
  GdkMonitor * monitor;
  monitor = gdk_display_get_monitor(pantalla,0);
  GdkRectangle rectangulo = {1,1};
  gdk_monitor_get_workarea(monitor,&rectangulo);
  ancho = rectangulo.width-50;
  alto = rectangulo.height-50;

    // Crear la ventana contenedora    
    createWind(&window, ancho, alto);
    gtk_window_maximize(GTK_WINDOW(window));
    gtk_window_set_title(GTK_WINDOW(window),"BackEnd        Xadnem 19");
    gtk_widget_set_name(window,"ventana");
    
    //    Crear el VBox para dividir verticalmente la ventana  
     createVBox(&VbDivisionesVentana,&window,"VbDivisionesVentana");
     gtk_widget_set_size_request(VbDivisionesVentana,ancho,alto);
     gtk_box_set_spacing(GTK_BOX(VbDivisionesVentana),10);
     gtk_box_set_homogeneous(GTK_BOX(VbDivisionesVentana),FALSE);

    //   Crear el HBox para la etiqueta de mensajes y el boton aceptar  
    createHBox(&HbMensajesDatos,&VbDivisionesVentana,"HbMensajesDatos");
    gtk_widget_set_size_request(HbMensajesDatos,ancho,alto/4);    

    //	Crear la etiqueta para los mensajes   
    lbMensajes = gtk_label_new(" Seleccione los directorios a respaldar y la unidad y directorio donde se realizará la\n copia de los mismos.");
    gtk_widget_set_name(lbMensajes,"lbMensajes");
    gtk_box_pack_start(GTK_BOX(HbMensajesDatos),lbMensajes,0,0,0);
    gtk_label_set_xalign(GTK_LABEL(lbMensajes),0);
    gtk_widget_set_size_request(lbMensajes,ancho-200,150);
    gtk_label_set_line_wrap_mode(GTK_LABEL(lbMensajes),PANGO_WRAP_WORD);
   
    //    Crear el Vbox para el boton de un nuevo calculo    
    VbBotonAceptar = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
    gtk_widget_set_valign(VbBotonAceptar,GTK_ALIGN_CENTER);

    //   Crear el boton para aceptar y establecer la configuracion introducida.  
    btAceptar = gtk_button_new_with_label("Aceptar");
    gtk_widget_set_name(btAceptar,"btAceptar");
    gtk_widget_set_size_request(btAceptar,100,50);
    gtk_box_pack_start(GTK_BOX(VbBotonAceptar),btAceptar,FALSE,FALSE,0);
    gtk_box_pack_end(GTK_BOX(HbMensajesDatos),VbBotonAceptar,0,0,25);
    g_signal_connect(G_OBJECT(btAceptar),"clicked",G_CALLBACK(on_btAceptar_clicked),NULL);

    // Crear el HBox para el boton añadir directorios, la listbox para los directorios a
    // respaldar y la combo box para la unidad de respaldo.
    createHBox(&HbDatos,&VbDivisionesVentana,"HbDatos");
    gtk_widget_set_size_request(HbDatos,ancho,200);
    gtk_box_set_spacing(GTK_BOX(HbDatos),20);
  
    //Crear el Vbox para el boton de añadir directorios
    VbBotonDirectorios = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
    gtk_widget_set_valign(VbBotonDirectorios,GTK_ALIGN_CENTER);

    // Crear el boton para añadir directorios 
    btDirectorios = gtk_button_new_with_label("Directorios\na respaldar");
    gtk_widget_set_name(btDirectorios,"btDirectorios");
    gtk_widget_set_size_request(btDirectorios,100,50);
    gtk_box_pack_start(GTK_BOX(VbBotonDirectorios),btDirectorios,FALSE,FALSE,0);
    gtk_box_pack_start(GTK_BOX(HbDatos),VbBotonDirectorios,FALSE,FALSE,0);
 g_signal_connect(G_OBJECT(btDirectorios),"clicked",G_CALLBACK(on_btDirectorios_clicked),NULL);
		    
 //Crear el Vbox para las etiquetas con las rutas de los directorios a salvar
   VbRutas = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
   gtk_box_set_homogeneous(GTK_BOX(VbRutas),1);
   gtk_widget_set_size_request(VbRutas,300,200);
   gtk_box_pack_start(GTK_BOX(HbDatos),VbRutas,FALSE,FALSE,0);

   //Iniciar el array de marcas de las etiquetas completas
   int i;
   for(i = 0; i < 5;i++)
      indices[i] = 0;
   
   //Crear las etiquetas para las rutas de los directorios a salvar
       lb1 = gtk_label_new("");
       lb2 = gtk_label_new("");
       lb3 = gtk_label_new("");
       lb4 = gtk_label_new("");
       lb5 = gtk_label_new("");
       gtk_widget_set_name(lb1,"lb1");
       gtk_widget_set_name(lb2,"lb2");
       gtk_widget_set_name(lb3,"lb3");
       gtk_widget_set_name(lb4,"lb4");
       gtk_widget_set_name(lb5,"lb5");
       gtk_label_set_xalign(GTK_LABEL(lb1),0);
       gtk_label_set_xalign(GTK_LABEL(lb2),0);
       gtk_label_set_xalign(GTK_LABEL(lb3),0);
       gtk_label_set_xalign(GTK_LABEL(lb4),0);
       gtk_label_set_xalign(GTK_LABEL(lb5),0);
      gtk_box_pack_start(GTK_BOX(VbRutas),lb1,FALSE,FALSE,0);
      gtk_box_pack_start(GTK_BOX(VbRutas),lb2,FALSE,FALSE,0);
      gtk_box_pack_start(GTK_BOX(VbRutas),lb3,FALSE,FALSE,0);
      gtk_box_pack_start(GTK_BOX(VbRutas),lb4,FALSE,FALSE,0);
      gtk_box_pack_start(GTK_BOX(VbRutas),lb5,FALSE,FALSE,0);
  
   //Crear los botones para eliminar rutas
   VbBotonesRutas = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
   bt1 = gtk_button_new_with_label("Quitar");
   bt2 = gtk_button_new_with_label("Quitar");
   bt3 = gtk_button_new_with_label("Quitar");
   bt4 = gtk_button_new_with_label("Quitar");
   bt5 = gtk_button_new_with_label("Quitar");
   gtk_widget_set_size_request(bt1,80,40);
   gtk_widget_set_size_request(bt2,80,40);
   gtk_widget_set_size_request(bt3,80,40);
   gtk_widget_set_size_request(bt4,80,40);
   gtk_widget_set_size_request(bt5,80,40);
    g_signal_connect(G_OBJECT(bt1),"clicked",G_CALLBACK(on_btEliminar_clicked),NULL);
    g_signal_connect(G_OBJECT(bt2),"clicked",G_CALLBACK(on_btEliminar_clicked),NULL);
    g_signal_connect(G_OBJECT(bt3),"clicked",G_CALLBACK(on_btEliminar_clicked),NULL);
    g_signal_connect(G_OBJECT(bt4),"clicked",G_CALLBACK(on_btEliminar_clicked),NULL);
    g_signal_connect(G_OBJECT(bt5),"clicked",G_CALLBACK(on_btEliminar_clicked),NULL);
   gtk_widget_set_name(bt1,"bt1");
   gtk_widget_set_name(bt2,"bt2");
   gtk_widget_set_name(bt3,"bt3");
   gtk_widget_set_name(bt4,"bt4");
   gtk_widget_set_name(bt5,"bt5");
   gtk_widget_set_visible(bt1,0);
    gtk_widget_set_visible(bt2,0);
    gtk_widget_set_visible(bt3,0);
    gtk_widget_set_visible(bt4,0);
    gtk_widget_set_visible(bt5,0);
   gtk_box_pack_start(GTK_BOX(VbBotonesRutas),bt1,FALSE,FALSE,0);
   gtk_box_pack_start(GTK_BOX(VbBotonesRutas),bt2,FALSE,FALSE,0);
   gtk_box_pack_start(GTK_BOX(VbBotonesRutas),bt3,FALSE,FALSE,0);
   gtk_box_pack_start(GTK_BOX(VbBotonesRutas),bt4,FALSE,FALSE,0);
   gtk_box_pack_start(GTK_BOX(VbBotonesRutas),bt5,FALSE,FALSE,0);
   gtk_box_pack_end(GTK_BOX(HbDatos),VbBotonesRutas,FALSE,FALSE,0);

   //Crear el Hbox para el boton para seleccionar la unidad de destino
   HbDestino = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);
   gtk_widget_set_size_request(HbDestino,ancho,250);
    gtk_widget_set_name(HbDestino,"HbDestino");
    gtk_box_set_spacing(GTK_BOX(HbDestino),20);
   gtk_box_pack_end(GTK_BOX(VbDivisionesVentana),HbDestino,FALSE,FALSE,0);

   //Crear el Vbox para el boton de añadir el directorio de destino
    VbBotonDestino = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
    gtk_widget_set_name(VbBotonDestino,"VbBotonDestino");
    gtk_widget_set_valign(VbBotonDestino,GTK_ALIGN_CENTER);
    // Crear el boton para añadir el directorio de destino
    btDestino = gtk_button_new_with_label(" Directorio   \n de destino");
    gtk_widget_set_name(btDestino,"btDestino");
    gtk_widget_set_size_request(btDestino,150,50);
    gtk_box_pack_start(GTK_BOX(VbBotonDestino),btDestino,FALSE,FALSE,0);
    gtk_box_pack_start(GTK_BOX(HbDestino),VbBotonDestino,FALSE,FALSE,0);
 g_signal_connect(G_OBJECT(btDestino),"clicked",G_CALLBACK(on_btDestino_clicked),NULL);
    
    //Crear la etiqueta para mostrar el directorio de destino    
    lbDestino = gtk_label_new("");
    gtk_label_set_xalign(GTK_LABEL(lbDestino),0);
    gtk_box_pack_end(GTK_BOX(HbDestino),lbDestino,FALSE,FALSE,0);
    gtk_widget_set_size_request(lbDestino,ancho-200,50);
    gtk_widget_set_name(lbDestino,"lbDestino");

    //Comprobar si existen los ficheros de texto y llenar las etiquetas para las rutas
    //con la informacion contenida en esos ficheros en caso afirmativo
    FILE * origenes = fopen("salvador/origenes.txt","r");
    FILE * destino = fopen("salvador/destino.txt","r");
    if(origenes != NULL && destino != NULL)
    {	    
      int i;
      char c,letra;
      size_t largo = 100;

      for(i = 0; i < 5; i++)
      {
	 letra = '0'+i;
	 char * lectura = (char*)malloc(100);
	 getline(&lectura,&largo,origenes);
	 GtkWidget * etiqueta, * boton;
	 switch(letra)
	 {
	   case '0':
		   etiqueta = lb1;
		   boton = bt1;
		   break;
	   case '1':
		   etiqueta = lb2;
		   boton = bt2;
		   break;
	   case '2':
		   etiqueta = lb3;
		   boton = bt3;
		   break;
	   case '3':
		   etiqueta = lb4;
		   boton = bt4;
		   break;
	   case '4':
		   etiqueta = lb5;
		   boton = bt5;
		   break;
	 }
	 int largolinea = strlen(lectura);
	 if(largolinea > 0 && lectura[largolinea-1] == '\n')
	 {
	   lectura[largolinea-1] = '\0';
	   gtk_label_set_text(GTK_LABEL(etiqueta),lectura);
	   gtk_widget_set_visible(boton,1);
	 }
      }
      char * lectura2 = (char*)malloc(100);
      getline(&lectura2,&largo,destino);
      int largolin = strlen(lectura2);
      if(largolin > 0 && lectura2[largolin-1] == '\n')
	      lectura2[largolin-1] = '\0';
      else
	      lectura2[largolin] = '\0';
      gtk_label_set_text(GTK_LABEL(lbDestino),lectura2);
      gtk_widget_set_visible(lbDestino,1);
      fclose(destino);
      fclose(origenes);
      remove("salvador/origenes.txt");
      remove("salvador/destino.txt");
    }
      
 
   //Mostrar solo los controles que no estan ocultos
    gtk_widget_show(window);
    gtk_widget_show_all(HbMensajesDatos);
    gtk_widget_show(VbDivisionesVentana);
    gtk_widget_show(VbBotonDirectorios);
    gtk_widget_show_all(VbBotonDirectorios);
    gtk_widget_show(HbDatos);
    gtk_widget_show_all(VbRutas);
    gtk_widget_show(VbBotonesRutas);
    gtk_widget_show(HbDestino);
    gtk_widget_show_all(VbBotonDestino);
    gtk_main();
    return 0;
}

///////////////////////////////////////////////////
//
//      GUARDA LA CONFIGURACION INTRODUCIDA EN FICHEROS DE TEXTO Y CIERRA LA VENTANA DE 
//      CONFIGURACION
//
///////////////////////////////////////////////////////////////////
//
void on_btAceptar_clicked()
{
   //Obtener la ruta del directorio de trabajo actual
   //char cwd[PATH_MAX];
   //getcwd(cwd,sizeof(cwd));
   //Crear los ficheros para guardar las rutas de los directorios a respaldar y de destino
   //char * no = "/salvador/origenes.txt";
   //char * nd = "/salvador/fdestino.txt";
   //char * origenes = (char*)malloc(PATH_MAX);
   //char * destino = (char*)malloc(PATH_MAX);
   //strcpy(origenes,cwd);
   //strcpy(destino,cwd);
   //strcat(origenes,no);
   //strcat(destino,nd);
   FILE * forigenes = fopen("salvador/origenes.txt","w");
   FILE * fdestino = fopen("salvador/destino.txt","w");
   //Llenar el fichero de texto con las rutas de los ficheros a respaldar
    const char * lectura1 = gtk_label_get_text(GTK_LABEL(lb1));
    int largo = strlen(lectura1);
    if(largo > 0)
    {
     fwrite(lectura1,1,largo,forigenes);
     fwrite("\n",1,1,forigenes);
    }
    const char * lectura2 = gtk_label_get_text(GTK_LABEL(lb2));
    largo = strlen(lectura2);
    if(largo > 0)
    {
     fwrite(lectura2,1,largo,forigenes);
     fwrite("\n",1,1,forigenes);
    }
    const char * lectura3 = gtk_label_get_text(GTK_LABEL(lb3));
    largo = strlen(lectura3);
    if(largo > 0)
    {
     fwrite(lectura3,1,largo,forigenes);
     fwrite("\n",1,1,forigenes);
    }
    const char * lectura4 = gtk_label_get_text(GTK_LABEL(lb4));
    largo = strlen(lectura4);
    if(largo > 0)
    {
     fwrite(lectura4,1,largo,forigenes);
     fwrite("\n",1,1,forigenes);
    }
    const char * lectura5 = gtk_label_get_text(GTK_LABEL(lb5));
    largo = strlen(lectura5);
    if(largo > 0)
    {
     fwrite(lectura5,1,largo,forigenes);
     fwrite("\n",1,1,forigenes);
    }
    fclose(forigenes);
    //Llenar el fichero de texto con la ruta del directorio de destino
    const char * lectura6 = gtk_label_get_text(GTK_LABEL(lbDestino));
    largo = strlen(lectura6);
    fwrite(lectura6,1,largo,fdestino);
    fclose(fdestino);
    //Cerrar la aplicacion
    gtk_widget_destroy(window);
}


///////////////////////////////////////////////////
//
//      MUESTRA EL DIALOGO PARA SELECCIONAR Y AÑADIR LOS DIRECTORIOS A RESPALDAR
//
///////////////////////////////////////////////////////////////////
//
void on_btDirectorios_clicked(GtkWidget *widget, gpointer data)
{
  //  Crear el File chooser dialog para los directorios a respaldar
    GtkFileChooserAction accion = GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER;
   gint respuesta; 
    dialogo = gtk_file_chooser_dialog_new("Open file",GTK_WINDOW(window),accion,("_Cancel"),GTK_RESPONSE_CANCEL,("_Open"),GTK_RESPONSE_ACCEPT,NULL);
    respuesta = gtk_dialog_run(GTK_DIALOG(dialogo));
    if(respuesta == GTK_RESPONSE_ACCEPT)
    {
       char * filename;
       GtkFileChooser * chooser = GTK_FILE_CHOOSER(dialogo);
       filename = gtk_file_chooser_get_filename(chooser);
       //Determinar el indice de la primera etiqueta vacia
       int i,indice,encontrado;
       encontrado = 0;//Para registrar si se ha encontrado una etiqueta vacia
       for(i = 0,indice = 1; i < 5;i++,indice++)
       {
	  if(indices[i] == 0)
	  {
		  encontrado = 1;
		  break;
	  }
       }
       if(encontrado)
       indices[indice-1] = 1;
       else
       {
	  gtk_widget_hide(btDirectorios);
	  gtk_widget_destroy(dialogo);
	  return;
       }
       switch(indice)
       {
	       case 1:
       		gtk_label_set_text(GTK_LABEL(lb1),filename);
		gtk_widget_show(bt1);
       		break;
	       case 2:
       		gtk_label_set_text(GTK_LABEL(lb2),filename);
		gtk_widget_show(bt2);
		break;
	       case 3:
       		gtk_label_set_text(GTK_LABEL(lb3),filename);
		gtk_widget_show(bt3);
		break;
	       case 4:
       		gtk_label_set_text(GTK_LABEL(lb4),filename);
		gtk_widget_show(bt4);
		break;
	       case 5:
       		gtk_label_set_text(GTK_LABEL(lb5),filename);
		gtk_widget_show(bt5);
		break;
       }
       indice++;
    }
gtk_widget_destroy(dialogo);
}

///////////////////////////////////////////////////
//
//      MUESTRA EL DIALOGO PARA SELECCIONAR Y AÑADIR EL DIRECTORIO DONDE SE GUARDARAN LOS
//      DIRECTORIOS A RESPALDAR
//
///////////////////////////////////////////////////////////////////
//
void on_btDestino_clicked(GtkWidget *widget, gpointer data)
{
//  Crear el File chooser dialog para los directorios a respaldar
    GtkFileChooserAction accion = GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER;
   gint respuesta; 
    dialogo = gtk_file_chooser_dialog_new("Open file",GTK_WINDOW(window),accion,("_Cancel"),GTK_RESPONSE_CANCEL,("_Open"),GTK_RESPONSE_ACCEPT,NULL);
    respuesta = gtk_dialog_run(GTK_DIALOG(dialogo));
    if(respuesta == GTK_RESPONSE_ACCEPT)
    {
       char * filename;
       GtkFileChooser * chooser = GTK_FILE_CHOOSER(dialogo);
       filename = gtk_file_chooser_get_filename(chooser);
       gtk_label_set_text(GTK_LABEL(lbDestino),filename);
       gtk_widget_show(lbDestino);
    }
    gtk_widget_destroy(dialogo);

}


///////////////////////////////////////////////////////////
//
//	APLICA LOS ESTILOS CONTENIDOS EN EL FICHERO .ccc A LOS WIDGETS
//
//////////////////////////////////////////////////////////////////
//
void myCSS(void){
    GtkCssProvider *provider;
    GdkDisplay *display;
    GdkScreen *screen;

    provider = gtk_css_provider_new ();
    display = gdk_display_get_default ();
    screen = gdk_display_get_default_screen (display);
    gtk_style_context_add_provider_for_screen (screen, GTK_STYLE_PROVIDER (provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    const gchar *myCssFile = "mystyle.css";
    GError *error = 0;

    gtk_css_provider_load_from_file(provider, g_file_new_for_path(myCssFile), &error);
    g_object_unref (provider);
}

////////////////////////////////////
//
//	ELIMINA LA ETIQUETA CON LA RUTA DEL DIRECTORIO AÑADIDO ANTERIORMENTE, OCULTA EL BOTON
//	CORRESPONDIENTE Y PONE EL FLAG DE ETIQUETA LIBRE A CERO
//
//////////////////////////////////////////////////
//
void on_btEliminar_clicked(GtkWidget *widget, gpointer data)
{
    const char * nombre = gtk_widget_get_name(widget);
    switch(nombre[2])
    {
      case '1':
	      gtk_widget_hide(bt1);
	      indices[0] = 0;
	      gtk_label_set_text(GTK_LABEL(lb1),"");
	      break;
      case '2':
	      gtk_widget_hide(bt2);
	      indices[1] = 0;
	      gtk_label_set_text(GTK_LABEL(lb2),"");
	      break;
      case '3':
	      gtk_widget_hide(bt3);
	      indices[2] = 0;
	      gtk_label_set_text(GTK_LABEL(lb3),"");
	      break;
      case '4':
	      gtk_widget_hide(bt4);
	      indices[3] = 0;
	      gtk_label_set_text(GTK_LABEL(lb4),"");
	      break;
      case '5':
	      gtk_widget_hide(bt5);
	      indices[4] = 0;
	      gtk_label_set_text(GTK_LABEL(lb5),"");
	      break;
    }

  gtk_widget_show(btDirectorios);

}





 
