#include<gtk/gtk.h>

/*		METODOS DE CREACION DE WIDGETS                  */

/* Crea un GtkWindow       en base al puntero a puntero GtkWidget pasado como primer argumento y el ancho y alto pasados como segundo y tercero 	*/
void createWind(GtkWidget **window, gint width, gint height); //Crear ventanas

/* Crean un GtkVbox u GtkHbox       respectivamente, en base al puntero a puntero GtkWidget pasado como primer argumento y el nombre pasado como segundo   */
void createVBox(GtkWidget **VBox, GtkWidget **Window, const gchar *name); //Crear VBoxes
void createHBox(GtkWidget **HBox, GtkWidget **Window, const gchar *name); //Crear HBoxes

/* Crea un GtkLabel en base al puntero a puntero GtkWidget pasado como primer argumento, el nombre pasado como segundo argumento, y el texto pasado como tercero    */
void createLabel(GtkWidget ** Label, GtkWidget ** Cont, const gchar *name,const gchar *lectura);

/* Crea un GtkLabel en base al puntero a puntero GtkWidget pasado como primer argumento, el
 * nombre pasado como segundo y el contenido pasado como tercero  */
void createLb(GtkWidget **Label,const char* nombre, const char* contenido);

/* Crea un GtkButton       en base al puntero a puntero GtkWidget pasado como primer argumento, el contenedor pasado como segundo , el nombre pasado como tercero y el texto pasado como cuarto   */
void createButton(GtkWidget** puntero, GtkWidget** contenedor,const gchar * nombre, gchar * label );

/* Crea un GtkGrid en base al puntero a puntero GtkWidget pasado como primer argumento, el contenedor pasado como segundo , el nombre pasado como tercero y las filas y columas pasadas como tercero y cuarto.  */
void createGrid(GtkWidget** puntero, GtkWidget** contenedor, const gchar *nombre,int filas,int columnas);

/* Crea un GtkEntry en base al puntero a puntero GtkWidget pasado como primer argumento, el contenedor pasado como segundo , el nombre pasado como tercero. */ 
void createEntry(GtkWidget** puntero, GtkWidget** contenedor, const char * nombre) ;

/* Crea un GtkEntry en base al puntero a puntero GtkWidget pasado como primer argumento y el
 * nombre pasado como segundo  */
void createTb(GtkWidget** puntero, const char* nombre);
