#include <gtk/gtk.h>
#include <stdio.h>


/************************************************************************
*
*	MEDOTOS DE CREACION DE WIDGETS PARA INTERFACES C		*
*									*
************************************************************************/

////////////////////////////////////////////////
//
//      CREA UNA VENTANA GtkWindow A PARTIR DEL PUNTERO A WIDGET PASADO COMO PRIMER ARGUMENTO
//      Y EL ANCHO Y ALTO PASADOS COMO SEGUNDO Y TERCER ARGUMENTO
//
////////////////////////////////////////////////////////////
//
void createWind(GtkWidget **window, gint width, gint height){
    *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    //gtk_window_set_title(GTK_WINDOW(*window), "InterpolacionNewton        xadnem 18");
    gtk_window_set_default_size(GTK_WINDOW(*window), width, height);
    gtk_window_set_resizable (GTK_WINDOW(*window), TRUE);
    gtk_container_set_border_width(GTK_CONTAINER(*window), 5);
    g_signal_connect(*window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
}


/////////////////////////////////////////////////////////
//
//      CREA UNA Gtk VBox A PARTIR DEL PUNTERO A WIDGET PASADO COMO PRIMER ARGUMENTO,
//      EL PUNTERO A WIDGET DE SU CONTENEDOR PASADO COMO SEGUNDO ARGUMENTO Y EL NOMBRE
//      PASADO COMO TERCERO
//
//////////////////////////////////////////////////////////
//
void createVBox(GtkWidget **VBox, GtkWidget **Window, const gchar *name){
    *VBox = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
    gtk_container_set_border_width(GTK_CONTAINER (*VBox), 15);
    gtk_widget_set_name(*VBox, name);
    gtk_container_add (GTK_CONTAINER (*Window), *VBox);
}
/////////////////////////////////////////////////////////
//
//      CREA UNA Gtk HBox A PARTIR DEL PUNTERO A WIDGET PASADO COMO PRIMER ARGUMENTO,
//      EL PUNTERO A WIDGET DE SU CONTENEDOR PASADO COMO SEGUNDO ARGUMENTO Y EL NOMBRE
//      PASADO COMO TERCERO
//
//////////////////////////////////////////////////////////
//
void createHBox(GtkWidget **HBox, GtkWidget **Window, const gchar *name){
    *HBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);
    gtk_container_set_border_width(GTK_CONTAINER (*HBox), 15);
    gtk_widget_set_name(*HBox, name);
    gtk_container_add (GTK_CONTAINER (*Window), *HBox);
}

/////////////////////////////////////////////////////////
//
//      CREA UNA Gtk Label A PARTIR DEL PUNTERO A WIDGET PASADO COMO PRIMER ARGUMENTO,
//      EL PUNTERO A WIDGET DE SU CONTENEDOR PASADO COMO SEGUNDO ARGUMENTO Y EL NOMBRE
//      PASADO COMO TERCERO
//
//////////////////////////////////////////////////////////
//
void createLabel(GtkWidget **Label, GtkWidget **Cont, const gchar *name, const gchar *lectura ){
    *Label = gtk_label_new(lectura);
    gtk_widget_set_name(*Label, name);
    gtk_container_add (GTK_CONTAINER (*Cont), *Label);
}

/////////////////////////////////////////////////////////
//
//	CREA UNA Gtk Label A PARTIR DEL PUNTERO A WIDGET PASADO COMO PRIMER ARGUMENTO,
//      EL NOMBRE PASADO COMO SEGUNDO Y EL CONTENIDO PASADO COMO TERCERO
//
//////////////////////////////////////////////////////////////////
//
void createLb(GtkWidget **Label, const char* nombre, const char * contenido)
{
	*Label = gtk_label_new(contenido);
	gtk_widget_set_name(*Label,nombre);
}

void btn_clicked(GtkWidget *widget, GtkEntry *entry){
    (void)widget;
    const gchar *gstrTexto;

    gstrTexto = gtk_entry_get_text(entry);
    g_print("%s\n", gstrTexto);
    gtk_editable_select_region(GTK_EDITABLE(entry) , 0, 3);
}

////////////////////////////////////////////////////
//
//      CREA UN BOTON EN BASE AL PUNTERO PASADO COMO PRIMER ARGUMENTO, EN EL CONTENEDOR PASADO
//      COMO SEGUNDO, CON EL NOMBRE PASADO COMO TERCERO Y LA ETIQUETA PASADA COMO CUARTO
//
//////////////////////////////////////////////////////////////////
//
void createButton(GtkWidget** puntero, GtkWidget** contenedor,const gchar * nombre, gchar * label )
{
    *puntero = gtk_button_new_with_label(label);
    gtk_widget_set_name(*puntero, nombre);
    gtk_widget_set_size_request(*puntero, 100, 35);
    g_object_set (*puntero, "margin", 5, NULL);
    gtk_container_add(GTK_CONTAINER(*contenedor),*puntero);
    //gtk_container_child_set_property(GTK_CONTAINER(*contenedor),*puntero,"expand",0);
}

////////////////////////////////////////////////////
//
//      CREA UNA CAJA DE TEXTO EN BASE AL PUNTERO PASADO COMO PRIMER ARGUMENTO, EN EL
//      CONTENEDOR PASADO COMO SEGUNDO, CON Y EL NOMBRE PASADO COMO TERCERO 
//
//////////////////////////////////////////////////////////////////
//
void createEntry(GtkWidget** puntero, GtkWidget** contenedor,const gchar * nombre )
{
    *puntero = gtk_entry_new();
    gtk_widget_set_name(*puntero, nombre);
    gtk_container_add(GTK_CONTAINER(*contenedor),*puntero);
}

////////////////////////////////////////////////////
//
//	CREA UN Gtk Entry EN BASE AL PUNTERO A PUNTERO A WIDGET PASADO COMO PRIMER ARGUMENTO
//	Y EL NOMBRE PASADO COMO SEGUNDO
//
//////////////////////////////////////////////////////////////
//
void createTb(GtkWidget** puntero, char * nombre)
{
	*puntero = gtk_entry_new();
	gtk_widget_set_name(*puntero,nombre);
}

///////////////////////////////////////////////////
//
//      CREA UN gtk Grid EN BASE AL PUNTERO PASADO COMO PRIMER ARGUMENTO, EN EL CONTENEDOR
//      PASADO COMO SEGUNDO ARGUMENTO Y CON EL NOMBRE PASADO COMO TERCERO
//
//////////////////////////////////////////////////////////////
//
void createGrid(GtkWidget** puntero, GtkWidget** contenedor, const gchar * nombre,int filas, int columnas)
{
   *puntero = gtk_grid_new();
   int i;
   //for(i = 0; i < filas;i++)
     //gtk_grid_insert_row(GTK_GRID(*puntero),i);
   //for(i = 0; i < columnas;i++)
     //gtk_grid_insert_column(GTK_GRID(*puntero),i);
   gtk_widget_set_name(*puntero,nombre);
   gtk_container_add(GTK_CONTAINER(*contenedor),*puntero);
   gtk_widget_set_size_request(*puntero,500,500);
}


