/**********************************************************************************************
 *											      *	
 *   SALVA LOS DIRECTORIOS CUYAS RUTAS ESTAN CONTENIDAS EN EL FICHERO DE TEXTO: origenes.txt  *
 *   EN EL DIRECTORIO CUYA RUTA ESTA CONTENIDA EN EL FICHERO DE TEXTO: destino.txt	      *
 *   GENERADOS POR configurador.c 							      *	 *											      *
 *											      *
 *********************************************************************************************/

#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>
#include<dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <utime.h>

#define PATH_MAX 4096

int RespaldarFicheros(char * directorio,char * destino);
int is_regular_file(const char *path);

int main()
{
   //Obtener la ruta del directorio donde se copiaran los directorios a respaldar
   char * destino = (char*)malloc(PATH_MAX);
   FILE* d = fopen("destino.txt","r");
   char c;
   int i;
   int contador = 0;
   while((c = fgetc(d)) != EOF)
     destino[contador++] = c;
   fclose(d);
   //Obtener la ruta de los directorios a respaldar
   FILE* o = fopen("origenes.txt","r");
   char ** origenes = (char**)malloc(sizeof(char*)*5);
   for(i = 0; i < 5; i++)
   {
     size_t l=0;
     origenes[i] = (char*)malloc(PATH_MAX);
     getline(&origenes[i],&l,o);
     //Eliminar el posible caracter de final de nueva linea
     int largo = strlen(origenes[i]);
     if(largo > 0 && origenes[i][largo-1] == '\n')
	origenes[i][largo-1] = '\0';
     //Respaldar el directorio si la ruta no esta vacia
     if(largo > 0)
      RespaldarFicheros(origenes[i],destino);
   }
   fclose(o);

      //Apagar el equipo una vez finalizada la copia
      system("shutdown -h now");
}


  /////////////////////////////////
  //
  //	HACE UNA COPIA DE TODOS LOS FICHEROS Y SUBDIRECTORIOS DEL DIRECTORIO CUYA RUTA SE
  //	PASA COMO PRIMER ARGUMENTO EN EL DIRECTORIO CUYA RUTA SE PASA COMO SEGUNDO
  //	
  ////////////////////////////////////////////////
  //
  int RespaldarFicheros(char * directorio, char * destino)
  {
    DIR * d;//Directorio en forma de objeto DIR
    struct dirent * fichero;//Para cada uno de los ficheros en forma de estructura dirent
    //Crear la ruta del directorio de destino
    char * dircopia = (char*)malloc(PATH_MAX);
    strcpy(dircopia,destino);
    //Obtener el nombre del directorio de origen sin el resto de su ruta
    char * DirectorioFinal(char * ruta);
    char * nombredir = DirectorioFinal(directorio);
    //printf("\nnombredir: %s\n",nombredir);
    //Concatenar la ruta de destino con el nombre del directorio a respaldar
    strcat(dircopia,nombredir);
    //Comprobar si el directorio a respaldar existe
    DIR* dir = opendir(dircopia);
    if (dir)
    {
     /* El directorio existe.Cerrarlo. */
	    //printf("El directorio a respaldar %s existe.\n",dircopia);
     closedir(dir);
    }
    else if (ENOENT == errno)
    {	    
     /* El directorio a respaldar no existe, crearlo */
     char * orden = (char*)malloc(PATH_MAX);
     //sprintf(orden,"sudo mkdir %s",dircopia);
     //system(orden);
     mkdir(dircopia,0777);
     //Asignar al directorio copia la fecha de modificacion del original
	struct stat atributosdiro;
	  stat(directorio,&atributosdiro);
	  //Obtener las fechas de modificacion del directorio original
	  time_t fechamoddiro = atributosdiro.st_mtime;
	 //Construir una estructura utimbuf con la fecha de modificacion del directorio origen
	  struct utimbuf auxi = { atributosdiro.st_ctime, fechamoddiro}; 
	//Cambiar la fecha de modificacion del directorio copia igual a la del origen
	   utime(dircopia,&auxi);
     //printf("Se crea el directorio de destino: %s\n",dircopia);  
    }
    //Abrir el directorio a respaldar
    if ((d = opendir (directorio)) != NULL)
    {
      //Leer el siguiente fichero y crear el objeto dirent
      while ((fichero = readdir (d)) != NULL) 
      {
        //Comprobar si se trata de un fichero simple o de un subdirectorio
	char * ruta = (char*)malloc(PATH_MAX);
	strcpy(ruta,directorio);
	strcat(ruta,"/");
	strcat(ruta,fichero->d_name);
	int esregular = is_regular_file(ruta);
	//Si se trata de un fichero regular, copiarlo en el destino
	if(esregular)
	{  
	  //Crear la ruta completa del fichero de destino	
	  char * rutacopia = (char*)malloc(PATH_MAX);
	  sprintf(rutacopia,dircopia);
	  strcat(rutacopia,"/");
          strcat(rutacopia,fichero->d_name);
          //Obtener las estructuras stat con los atributos de los ficheros de origen y destino
	  struct stat atributosd, atributoso;
	  int existe = stat(rutacopia,&atributosd);//stat devuelve -1 si no encuentra el fichero
	  stat(ruta,&atributoso);
	  //Obtener las fechas de modificacion de los ficheros de origen y destino
	  time_t fechamodo = atributoso.st_mtime;
	  time_t fechadest = atributosd.st_mtime;
	  //Si el fichero existe en el destino, comprobar si es mas antiguo que
	  //el del origen y si no lo es no hacer nada
	     if(existe == 0 && fechamodo == fechadest)
	     {
	        continue;
	     }
	   else
	   {
	   //Copiar el fichero en la ruta de origen en la ruta de destino
	   printf("\nCopiando: %s en: %s\n",ruta,rutacopia);
	   //char p = getchar();
	   //system(orden);
	   FILE * forigen = fopen(ruta,"rb");
	   FILE * fcopia = fopen(rutacopia,"wb");
	   char c;
	   if(forigen != NULL && fcopia != NULL)
	   {
            size_t n, m;
	    unsigned char buff[8192];
	    do 
	    {
    	      n = fread(buff, 1, sizeof buff, forigen);
    	      if (n)
		m = fwrite(buff, 1, n, fcopia);
    	      else 
		m = 0;
	    } 
	    while ((n > 0) && (n == m));
	    if (m)
	      perror("copy");
	  }
	  //Construir una estructura utimbuf con la fecha de modificacion del fichero de origen
	  struct utimbuf aux = { atributoso.st_ctime, fechamodo}; 
	  int check;//Para depuracion
	  //check = utime(rutacopia,&aux);
	  //stat(rutacopia,&atributosd);
	  //fechadest = atributosd.st_mtime;
	  //Cerrar los ficheros de origen y destino
	  if(forigen != NULL)
	   fclose(forigen);
	  if(fcopia != NULL)
	   fclose(fcopia);
	  //Cambiar la fecha de modificacion del fichero copia igual a la del fichero origen
	  check = utime(rutacopia,&aux);
	  stat(rutacopia,&atributosd);//para depuracion
	  fechadest = atributosd.st_mtime;//para depuracion
	 }
	}
	else//Si se trata de un directorio, hacer una llamada recursiva a este metodo
	{
	  if(fichero->d_name[0] != '.') //Si no es un directorio padre
	  {
	    //Construir la ruta completa de directorio a respaldar
	    char * nuevoorigen = (char*)malloc(PATH_MAX);
	    strcpy(nuevoorigen,directorio);
	    strcat(nuevoorigen,"/");
	    strcat(nuevoorigen,fichero->d_name);
	    //Construir la ruta completa del directorio de destino
	    char * nuevodestino = (char*)malloc(PATH_MAX);
	    strcpy(nuevodestino,destino);
	    strcat(nuevodestino,nombredir);
	    printf("\nNueva llamada: RespaldarFicheros(%s %s)",nuevoorigen,nuevodestino);
	    //char c = getchar();
	    RespaldarFicheros(nuevoorigen,nuevodestino);
	  }
	}
      }
      closedir (d);
    } 
    else/* Si no se puede abrir el directorio */
    {
     //perror ("");
     //return EXIT_FAILURE;
     printf("\nNo se ha podido abrir el directorio.");
     return -1;
    }
    return 0;
  }

  /////////////////////////////////////
  //
  //	DEVUELVE CERO SI EL FICHERO CUYA RUTA SE PASA COMO ARGUMENTO ES UN FICHERO
  //	REGULAR Y FALSE SI ES UN DIRECTORIO
  //
  //////////////////////////////////////////////
  //
  int is_regular_file(const char *path)
  {
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISREG(path_stat.st_mode);
  }
 
  ///////////////////////////////////
  //
  //	DEVUELVE EL NOMBRE DEL ULTIMO DIRECTORIO EN LA RUTA PASADA COMO ARGUMENTO
  //
  /////////////////////////////////////////////
  //
  char * DirectorioFinal(char * ruta)
  {
     int largo = strlen(ruta);
     int i,j;
     char c;
     //Obtener el indice de la ultima contrabarra en la ruta
     for(i = largo-1; (c = ruta[i]) != '/';i--)
	     ;
     char * nombre = (char*)malloc(1);
     for(j = 0; i < largo;i++,j++)
     {
	     nombre[j] = ruta[i];
	     nombre = (char*)realloc(nombre,j+2);
     }       
     nombre[j] = '\0';
     return nombre;
  }
