Esta aplicación se compone de dos programas:

   - <u>**configurador**:</u> Permite seleccionar los directorios a respaldar y la ruta de destino. Una vez se pulsa el botón aceptar, la configuración se guarda en un fichero de texto.
   - <u>**salvador**:</u> Lee el fichero de configuración y realiza la copia de los directorios en la ruta allí escrita.

Una vez finalizada la copia, el sistema se apaga. Si no se desea este comportamiento, hay que comentar la línea 55 del fichero salvador.c
